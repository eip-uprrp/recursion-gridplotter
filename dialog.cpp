#include "dialog.h"
#include "ui_dialog.h"
#include "grid.cpp"

#include <QtGui>
#include <QtCore>
#include <QWidget>
#include <QGridLayout>

/// \fn Dialog::Dialog(QWidget *parent)
/// \~English
/// \brief The constructor sets the undo and redo to disable so it cant be pressed
/// also sets the connections between the buttons and their respective signals.
/// \~Spanish
/// \brief El constructor ajusta el boton de deshacer (undo) y rehacer (redo) para que no
/// puedan ser apretados, tambien ajusta las conecciones entre los botones y sus senales.
Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    ui->undoButton->setDisabled(true);
    ui->redoButton->setDisabled(true);

    connect(ui->mainGrid, SIGNAL(canUndo(bool)), this, SLOT(setUndo(bool)));
    connect(ui->mainGrid, SIGNAL(canRedo(bool)), this, SLOT(setRedo(bool)));
}

/// \fn void Dialog::setUndo(bool can)
/// \~English
/// \brief  Enables the undo button
/// \~Spanish
/// \brief Permite utilizar el boton de deshacer (undo)
void Dialog::setUndo(bool can){
    if (can == true) ui->undoButton->setDisabled(false);
    else ui->undoButton->setDisabled(true);
}

/// \fn void Dialog::setRedo(bool can)
/// \~English
/// \brief  Enables the redo button
/// \~Spanish
/// \brief Permite utilizar el boton de rehacer (redo)
void Dialog::setRedo(bool can){
    if (can == true) ui->redoButton->setDisabled(false);
    else ui->redoButton->setDisabled(true);
}

/// \fn Dialog::~Dialog()
/// \~English
/// \brief Destructor
/// \~Spanish
/// \brief Destructor
Dialog::~Dialog()
{
    delete ui;
}

/// \fn void Dialog::on_frontcolorComboBox_activated(const QString &color)
/// \~English
/// \brief Sets the color of the brush
/// \param color color of the brush
/// \~Spanish
/// \brief Ajusta el color de la brocha
/// \param color color de la brocha
void Dialog::on_frontcolorComboBox_activated(const QString &color)
{
    ui->mainGrid->setFront(color);
}


/// \fn void Dialog::on_backgroundComboBox_activated(const QString &color)
/// \~English
/// \brief Sets the color of the background
/// \param color color del background
/// \~Spanish
/// \brief Ajusta el color del fondo
/// \param color color del fondo
void Dialog::on_backgroundComboBox_activated(const QString &color)
{
    ui->mainGrid->setBack(color);
    ui->mainGrid->repaint();
}

/// \fn void Dialog::on_lapizButton_clicked()
/// \~English
/// \brief Sets the tool to dot
/// \~Spanish
/// \brief Ajusta la herramienta a usar la punta
void Dialog::on_lapizButton_clicked()
{
    ui->mainGrid->setTool("dot");
}

/// \fn void Dialog::on_cleargridButton_clicked()
/// \~English
/// \brief Clears the grid. Erase the grid
/// \~Spanish
/// \brief Limpia la cuadricula Borra la cuadricula.
void Dialog::on_cleargridButton_clicked()
{
    ui->mainGrid->clear();
    ui->mainGrid->repaint();
}

/// \fn void Dialog::on_rowmajorfillButton_clicked()
/// \~English
/// \brief Sets the tool to Row fill
/// \~Spanish
/// \brief Ajusta la herramienta a llenar fila
void Dialog::on_rowmajorfillButton_clicked()
{
    ui->mainGrid->setTool("row");
}

/// \fn void Dialog::on_colmajorfillButton_clicked()
/// \~English
/// \brief Sets the tool to Column fill
/// \~Spanish
/// \brief Ajusta la herramienta a llenar columna
void Dialog::on_colmajorfillButton_clicked()
{
    ui->mainGrid->setTool("column");
}

/// \fn void Dialog::on_diagonalleftButton_clicked()
/// \~English
/// \brief Sets the tool to Diagonal Left fill
/// \~Spanish
/// \brief Ajusta la herrramienta a llenar la diagonal hacia la izquierda
void Dialog::on_diagonalleftButton_clicked()
{
    ui->mainGrid->setTool("diagonal left");
}

/// \fn void Dialog::on_diagonalrightButton_clicked()
/// \~English
/// \brief Sets the tool to Diagonal Right fill
/// \~Spanish
/// \brief Ajusta la herrramienta a llenar la diagonal hacia la derecha
void Dialog::on_diagonalrightButton_clicked()
{
    ui->mainGrid->setTool("diagonal right");
}

/// \fn void Dialog::on_drawsquareButton_clicked()
/// \~English
/// \brief Sets the tool to paint Squares
/// \~Spanish
/// \brief Ajusta la herramiento a pintar cuadrados
void Dialog::on_drawsquareButton_clicked()
{
    ui->mainGrid->setTool("square");
}

/// \fn void Dialog::on_drawtriangleButton_clicked()
/// \~English
/// \brief Sets the tool to paint Triangles
/// \~Spanish
/// \brief Ajusta la herramiento a pintar triangulos
void Dialog::on_drawtriangleButton_clicked()
{
    ui->mainGrid->setTool("triangle");
}

/// \fn void Dialog::on_drawcircleButton_clicked()
/// \~English
/// \brief Sets the tool to paint Circles
/// \~Spanish
/// \brief Ajusta la herramiento a pintar circulos
void Dialog::on_drawcircleButton_clicked()
{
    ui->mainGrid->setTool("circle");
}

/// \fn void Dialog::on_speedHorizontalSlider_valueChanged(int value)
/// \~English
/// \brief Sets the tool point size
/// \param value point size
/// \~Spanish
/// \brief Ajusta el tamano de la punta de la herramienta
/// \param value tamano de la punta
void Dialog::on_speedHorizontalSlider_valueChanged(int value)
{
    ui->mainGrid->setToolSize(value);
    ui->lblSize->setText(QString::number(value));
}

/// \fn void Dialog::on_undoButton_clicked()
/// \~English
/// \brief Undo the previous grid modification
/// \~Spanish
/// \brief Deshace la modificacion previa de la cuadricula
void Dialog::on_undoButton_clicked()
{
    ui->mainGrid->undo();
}

/// \fn void Dialog::on_redoButton_clicked()
/// \~English
/// \brief Redo the previous grid modification
/// \~Spanish
/// \brief Rehace la modificacion previa de la cuadricula
void Dialog::on_redoButton_clicked()
{
    ui->mainGrid->redo();
}

void Dialog::on_floodButton_clicked()
{
    ui->mainGrid->setTool("flood");
}
