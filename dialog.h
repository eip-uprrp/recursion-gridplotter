#ifndef DIALOG_H
#define DIALOG_H

#include "grid.h"
#include <QDialog>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:

    /// \fn Dialog::Dialog(QWidget *parent)
    /// \~English
    /// \brief The constructor sets the undo and redo to disable so it cant be pressed
    /// also sets the connections between the buttons and their respective signals.
    /// \~Spanish
    /// \brief El constructor ajusta el boton de deshacer (undo) y rehacer (redo) para que no
    /// puedan ser apretados, tambien ajusta las conecciones entre los botones y sus senales.
    explicit Dialog(QWidget *parent = 0);

    /// \fn Dialog::~Dialog()
    /// \~English
    /// \brief Destructor
    /// \~Spanish
    /// \brief Destructor
    ~Dialog();

private slots:

    /// \fn void Dialog::on_frontcolorComboBox_activated(const QString &color)
    /// \~English
    /// \brief Sets the color of the brush
    /// \param color color of the brush
    /// \~Spanish
    /// \brief Ajusta el color de la brocha
    /// \param color color de la brocha
    void on_frontcolorComboBox_activated(const QString &color);

    /// \fn void Dialog::on_backgroundComboBox_activated(const QString &color)
    /// \~English
    /// \brief Sets the color of the background
    /// \param color color del background
    /// \~Spanish
    /// \brief Ajusta el color del fondo
    /// \param color color del fondo
    void on_backgroundComboBox_activated(const QString &color);

    /// \fn void Dialog::on_lapizButton_clicked()
    /// \~English
    /// \brief Sets the tool to dot
    /// \~Spanish
    /// \brief Ajusta la herramienta a usar la punta
    void on_lapizButton_clicked();

    /// \fn void Dialog::on_cleargridButton_clicked()
    /// \~English
    /// \brief Clears the grid. Erase the grid
    /// \~Spanish
    /// \brief Limpia la cuadricula Borra la cuadricula.
    void on_cleargridButton_clicked();

    /// \fn void Dialog::on_rowmajorfillButton_clicked()
    /// \~English
    /// \brief Sets the tool to Row fill
    /// \~Spanish
    /// \brief Ajusta la herramienta a llenar fila
    void on_rowmajorfillButton_clicked();


    /// \fn void Dialog::on_colmajorfillButton_clicked()
    /// \~English
    /// \brief Sets the tool to Column fill
    /// \~Spanish
    /// \brief Ajusta la herramienta a llenar columna
    void on_colmajorfillButton_clicked();

    /// \fn void Dialog::on_diagonalleftButton_clicked()
    /// \~English
    /// \brief Sets the tool to Diagonal Left fill
    /// \~Spanish
    /// \brief Ajusta la herrramienta a llenar la diagonal hacia la izquierda
    void on_diagonalleftButton_clicked();

    /// \fn void Dialog::on_diagonalrightButton_clicked()
    /// \~English
    /// \brief Sets the tool to Diagonal Right fill
    /// \~Spanish
    /// \brief Ajusta la herrramienta a llenar la diagonal hacia la derecha
    void on_diagonalrightButton_clicked();

    /// \fn void Dialog::on_drawsquareButton_clicked()
    /// \~English
    /// \brief Sets the tool to paint Squares
    /// \~Spanish
    /// \brief Ajusta la herramiento a pintar cuadrados
    void on_drawsquareButton_clicked();

    /// \fn void Dialog::on_drawtriangleButton_clicked()
    /// \~English
    /// \brief Sets the tool to paint Triangles
    /// \~Spanish
    /// \brief Ajusta la herramiento a pintar triangulos
    void on_drawtriangleButton_clicked();

    /// \fn void Dialog::on_drawcircleButton_clicked()
    /// \~English
    /// \brief Sets the tool to paint Circles
    /// \~Spanish
    /// \brief Ajusta la herramiento a pintar circulos
    void on_drawcircleButton_clicked();

    /// \fn void Dialog::on_speedHorizontalSlider_valueChanged(int value)
    /// \~English
    /// \brief Sets the tool point size
    /// \param value point size
    /// \~Spanish
    /// \brief Ajusta el tamano de la punta de la herramienta
    /// \param value tamano de la punta
    void on_speedHorizontalSlider_valueChanged(int value);

    /// \fn void Dialog::on_undoButton_clicked()
    /// \~English
    /// \brief Undo the previous grid modification
    /// \~Spanish
    /// \brief Deshace la modificacion previa de la cuadricula
    void on_undoButton_clicked();

    /// \fn void Dialog::on_redoButton_clicked()
    /// \~English
    /// \brief Redo the previous grid modification
    /// \~Spanish
    /// \brief Rehace la modificacion previa de la cuadricula
    void on_redoButton_clicked();

    /// \fn void Dialog::setUndo(bool can)
    /// \~English
    /// \brief  Enables the undo button
    /// \~Spanish
    /// \brief Permite utilizar el boton de deshacer (undo)
    void setUndo(bool cannot);

    /// \fn void Dialog::setRedo(bool can)
    /// \~English
    /// \brief  Enables the redo button
    /// \~Spanish
    /// \brief Permite utilizar el boton de rehacer (redo)
    void setRedo(bool cannot);

    void on_floodButton_clicked();

private:
    Ui::Dialog *ui;
    GridWidget *grid;
};

#endif // DIALOG_H
