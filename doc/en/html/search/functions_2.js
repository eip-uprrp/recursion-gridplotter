var searchData=
[
  ['getcellcolor',['getCellColor',['../class_grid_widget.html#a50a2a9b2490de84dba08be23130fd9d4',1,'GridWidget']]],
  ['getgridcolumns',['getGridColumns',['../class_grid_widget.html#a1b92c3f484ea93338f3c0b8348a334f9',1,'GridWidget']]],
  ['getgridrows',['getGridRows',['../class_grid_widget.html#afa1e34f8dbae400cf200fdeb2398f138',1,'GridWidget']]],
  ['gridwidget',['GridWidget',['../class_grid_widget.html#ad3370c8905e4f03b666d1be221f4c412',1,'GridWidget::GridWidget(int rowCount=100, int columnCount=100, QWidget *parent=0)'],['../class_grid_widget.html#a8d85ee082bf0dd38eb581697844f825d',1,'GridWidget::GridWidget(QWidget *parent)']]]
];
