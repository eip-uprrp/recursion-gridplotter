#include "grid.h"
#include <QtGlobal>
#include <iostream>

using namespace std;

// Receives the x,y coordinates of the grid where the user clicked and paints
// that cell with the color of the tool.

void GridWidget::Dot(int x, int y, QColor toolColor){

    // Switch/paints on the cell in position x, y
    switchOn(x, y, toolColor);
}

// Receives the x,y grid coordinates and the color of cell where the user
// clicked. Paints the row (from that point left and right) of color toolColor until
// it hits a cell of a different color than the one clicked.

void GridWidget::RowMajorFill(int x, int y, QColor colorClicked, QColor toolColor){
     for (int i = x; i<getGridColumns() && (getCellColor(i,y) == colorClicked) ; i++)
            switchOn(i, y, toolColor);
    
     for (int i = (x-1); i>=0 && (getCellColor(i,y) == colorClicked) ; i--)
            switchOn(i, y, toolColor);
}


// Receives the grid x,y coordinates and the color of cell where the user
// clicked. Paints the column (from that point up and down) of color toolColor until
// it hits a cell of a different color than the one clicked.

void GridWidget::ColMajorFill(int x, int y, QColor colorClicked, QColor toolColor){


}

// Receives the grid x,y coordinates and the color of cell where the user
// clicked. Paints a left-diagonal of color toolColor until it hits a cell
// of a different color than the one clicked.

void GridWidget::DiagonalLeft(int x, int y, QColor colorClicked, QColor toolColor){


}

// Receives the grid x,y coordinates and the color of the cell where the user
// clicked. Paints a right-diagonal of color toolColor until it hits a cell
// of a different color than the one clicked.

void GridWidget::DiagonalRight(int x, int y, QColor colorClicked, QColor toolColor){


}


// Paints at x,y a square of the specified size and toolcolor.

void GridWidget::square(int x, int y, QColor toolColor, int toolSize){


}


// Paints at x,y a triangle of the specified size and toolcolor.

void GridWidget::triangle(int x, int y, QColor toolColor, int toolSize){


}

// Paints at x,y a circle of the specified size and toolcolor.

void GridWidget::circle(int x, int y, QColor toolColor, int toolSize){


}

// Floods the grid with the toolcolor starting at cell x,y (whose color is ColorClicked)

void GridWidget::flood_fill(int x, int y, QColor toolColor, QColor ColorClicked){


}
